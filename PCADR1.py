import pandas as pd

folder = '.\\'
csvFile = folder + r'PointZero.csv'
print("CSV File: ", csvFile)

csvData = pd.read_csv(csvFile, sep=',', encoding='utf_8')

df = pd.DataFrame(csvData)
print("DF head: \n", df.head(), "\n")
print("DF shape: ", df.shape, "\n")
print("DF keys: \n", df.keys(), "\n")

from sklearn import preprocessing

df.fillna(0, inplace=True)
le = preprocessing.LabelEncoder()
df['OS version and architecture'] = le.fit_transform(df['OS version and architecture'])
df['Windows Update status'] = le.fit_transform(df['Windows Update status'])
df.drop(['Engine', 'All antispyware', 'All antiviruses'], 1, inplace = True)
print("DF head: ", df.head(), "\n")

# import numpy as np

# for column in df:
#     ary = np.array(df[column])
#     ary = preprocessing.normalize([ary])
#     df[column] = ary.flatten()

# print(df.head(10))

x = df.drop('Number of system crashes', axis = 1)
y = df['Number of system crashes']

print("x: ", x.shape, "\n")
print("y: ", y.shape, "\n")

from sklearn.model_selection import train_test_split

# x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = 0, stratify = y)
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state = 88)

from sklearn.feature_selection import VarianceThreshold

constant_filter = VarianceThreshold(threshold = 0.01)
constant_filter.fit(x_train)
x_train_filter = constant_filter.transform(x_train)
x_test_filter = constant_filter.transform(x_test)
print("x_train_filter: ", x_train_filter.shape, " / x_test_filter: ", x_test_filter.shape)

x_train_t = x_train_filter.T
x_test_t = x_test_filter.T
x_train_t = pd.DataFrame(x_train_t)
x_test_t = pd.DataFrame(x_test_t)

print("duplicated sum: ", x_train_t.duplicated().sum(), "\n")

duplicated_features = x_train_t.duplicated()

print("duplicated features: ", duplicated_features.shape, "\n")

features_to_keep = [not index for index in duplicated_features]

print("features to keep: ", features_to_keep, "\n") 

x_train_unique = x_train_t[features_to_keep].T
x_test_unique = x_test_t[features_to_keep].T

from sklearn.preprocessing import StandardScaler

scaler = StandardScaler().fit(x_train_unique)
x_train_unique = scaler.transform(x_train_unique)
x_test_unique = scaler.transform(x_test_unique)
x_train_unique = pd.DataFrame(x_train_unique)
x_test_unique = pd.DataFrame(x_test_unique)
print("x train unique: ", x_train_unique.shape, " / x test unique: ", x_test_unique.shape, "\n")

corrmat = x_train_unique.corr()
print("corrmat: ", corrmat.shape, "\n")

def get_correlation(data, threshold):
    corr_col = set()
    corrmat = data.corr()
    for i in range(len(corrmat.columns)):
        for j in range(i):
            if abs(corrmat.iloc[i, j]) > threshold:
                colname = corrmat.columns[i]
                corr_col.add(colname)
    return corr_col

corr_features = get_correlation(x_train_unique, 0.70)
print("correlated features: ", len(set(corr_features)), "\n")

x_train_uncorr = x_train_unique.drop(labels = corr_features, axis = 1)
x_test_uncorr = x_test_unique.drop(labels = corr_features, axis = 1)
print("x train uncorr: ", x_train_uncorr.shape, " / x test uncorr: ", x_test_uncorr.shape, "\n")

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA

unique_classes = len(set(y)) - 1
print("LDA unique classes: ", unique_classes, "\n")

lda = LDA(n_components = unique_classes)
x_train_lda = lda.fit_transform(x_train_uncorr, y_train)
print("x train lda: ", x_train_lda.shape, "\n")

x_test_lda = lda.transform(x_test_uncorr)

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, roc_auc_score

def run_randomForest(x_train, x_test, y_train, y_test):
    clf = RandomForestClassifier(n_estimators = 100, random_state = 0, n_jobs = -1)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)
    print("Acurracy on test set: ", accuracy_score(y_test, y_pred), "\n")

run_randomForest(x_train_lda, x_test_lda, y_train, y_test)

run_randomForest(x_train, x_test, y_train, y_test)

from sklearn.decomposition import PCA

# no reduction in dimensions
# dimensions_to_reduce_to = len(x_train_uncorr.columns)
# print("PCA feature dimensions: ", dimensions_to_reduce_to, "\n")
# reduce dimensions to 2
dimensions_to_reduce_to = 2

pca = PCA(n_components = dimensions_to_reduce_to, random_state = 42)
pca.fit(x_test_uncorr)

x_train_pca = pca.transform(x_train_uncorr)
x_test_pca = pca.transform(x_test_uncorr)

print("x train pca: ", x_train_pca.shape, " / x train uncorr: ", x_train_uncorr.shape, " / x test pca: ", x_test_pca.shape, " / x test uncorr: ", x_test_uncorr.shape, "\n")

run_randomForest(x_train_pca, x_test_pca, y_train, y_test)
feature_components = 3 #len(x_train_uncorr.columns)
print("Features: ", feature_components, "\n")

for component in range(1, feature_components):
    pca = PCA(n_components = component, random_state = 42)
    pca.fit(x_test_uncorr)
    x_train_pca = pca.transform(x_train_uncorr)
    x_test_pca = pca.transform(x_test_uncorr)
    print("Selected component: ", component)
    run_randomForest(x_train_pca, x_test_pca, y_train, y_test)
    print()

print(x_test_pca)
print("EXECUTION COMPLETE!!!")