import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn import preprocessing
import matplotlib.pyplot as plt

folder = '.\\'
csvFile = folder + r'PointZero.csv'

csvData = pd.read_csv(csvFile, sep=',', encoding='utf_8')

data = pd.DataFrame(csvData)

print(data.head())
print(data.shape)

le = preprocessing.LabelEncoder()
data['OS version and architecture'] = le.fit_transform(data['OS version and architecture'])
data['Windows Update status'] = le.fit_transform(data['Windows Update status'])
#data['All antispyware'] = le.fit_transform(data['All antispyware'])
#data['All antiviruses'] = le.fit_transform(data['All antiviruses'])
data.fillna(0, inplace=True)
data.drop(['Engine', 'All antispyware', 'All antiviruses'], 1, inplace=True)
print(data.head())

# os_array = np.array(data['OS version and architecture'])
# os_array = preprocessing.normalize([os_array])
# data['OS version and architecture'] = os_array.flatten()

# update_array = np.array(data['Windows Update status'])
# update_array = preprocessing.normalize([update_array])
# data['Windows Update status'] = update_array.flatten()

for column in data:
    ary = np.array(data[column])
    ary = preprocessing.normalize([ary])
    data[column] = ary.flatten()

print(data.head())
#scaled_data = preprocessing.scale(data.T)
#print(scaled_data.head())
scaler = preprocessing.StandardScaler()
scaled_data = scaler.fit_transform(data.T)
scaled_data = pd.DataFrame(scaled_data)

pca = PCA()
pca.fit(scaled_data)
pca_data = pca.transform(scaled_data)

per_var = np.round(pca.explained_variance_ratio_ * 100, decimals=1)
labels = data.keys()
# print(labels)

plt.bar(x = range(1, len(per_var) + 1), height = per_var, tick_label = labels)
plt.ylabel('Percentage of Explained Variance')
plt.xlabel('Principal Component')
plt.title('Scree Plot')
plt.show()

pca_df = pd.DataFrame(pca_data, columns=labels)

plt.scatter(pca_df['OS version and architecture'], pca_df['Total active days'])
plt.title('My PCA Graph')
# plt.xlabel('OS version and architecture - {0}%', format(per_var[0]))
# plt.ylabel('Total active days - {0}%', format(per_var[1]))
plt.xlabel('OS ' + str(per_var[0]))
plt.ylabel('Active days ' + str(per_var[1]))

for sample in pca_df.index:
    plt.annotate(sample, (pca_df['OS version and architecture'].loc[sample], pca_df['Total active days'].loc[sample]))

plt.show()

loading_scores = pd.Series(pca.components_[0])
sorted_loading_scores = loading_scores.abs().sort_values(ascending=False)

top_10 = sorted_loading_scores[0:10].index.values
print(loading_scores[top_10])