# Uses PCA.py

import pandas as pd

folder = '.\\'
csvFile = folder + r'PointZero.csv'
print("CSV File: ", csvFile)

LabelTargetName = 'Number of system crashes'

csvData = pd.read_csv(csvFile, sep=',', encoding='utf_8')
labels = csvData[LabelTargetName]
csvData = csvData.drop(columns = [LabelTargetName])

from PCA import ReduceDimensions
fs = ReduceDimensions(data = csvData, labels = labels)

fs.identify_all(selection_params = {'missing_threshold': 0.6,    
                                    'correlation_threshold': 0.98, 
                                    'task': 'regression',    
                                    'eval_metric': 'auc', 
                                    'cumulative_importance': 0.99})

print('SAMPLE:\n', fs.feature_importances.head(), '\n')
top_principal_components = 17
fs.plot_feature_importances(threshold = 0.99, plot_n = top_principal_components)
my_selected_features = list(fs.feature_importances.loc[:top_principal_components, 'feature'])
print(my_selected_features)