import pandas as pd
# from sklearn.decomposition import PCA
# import matplotlib.pyplot as plt

folder = '.\\'
csvFile = folder + r'PointZero.csv'

csvData = pd.read_csv(csvFile, sep=',', encoding='utf_8')

df = pd.DataFrame(csvData)
print('DF')
print(df.head())
print(df.shape)
print(df.keys())

from sklearn import preprocessing

df.fillna(0, inplace=True)
le = preprocessing.LabelEncoder()
df['OS version and architecture'] = le.fit_transform(df['OS version and architecture'])
df['Windows Update status'] = le.fit_transform(df['Windows Update status'])
# data['All antispyware'] = le.fit_transform(df['All antispyware'])
# data['All antiviruses'] = le.fit_transform(df['All antiviruses'])
# df.drop(['Engine'], inplace=True)
df.drop(['Engine', 'All antispyware', 'All antiviruses'], 1, inplace=True)
print(df.head())

# import numpy as np

# for column in df:
#     ary = np.array(df[column])
#     ary = preprocessing.normalize([ary])
#     df[column] = ary.flatten()

X = df.drop('Number of system crashes', axis=1)
print('X')
print(X.head())
print(X.shape)
print(X.keys())

Y = df['Number of system crashes']
print('Y')
print(Y.head())
print(Y.shape)
print(Y.keys())

from sklearn.preprocessing import StandardScaler
x_std = StandardScaler().fit_transform(X)
print('X Std: {x_std}\n')

import numpy as np
features = x_std.T
covariance_matrix = np.cov(features)
print('Covariance Matrix: {covariance_matrix}\n')

eig_vals, eig_vecs = np.linalg.eig(covariance_matrix)
print('Eigen Vectors \n%s' %eig_vecs)
print('Eigen Values \n%s' %eig_vals)

variance = eig_vals[0] / sum(eig_vals)
print('Variance: {variance}\n')

projected_X = x_std.dot(eig_vecs.T[0])
print('Projected X: {projected_X}\n')

result = pd.DataFrame(projected_X, columns=['PC1'])
result['y-axis'] = 0.0
result['label'] = Y
print('Result')
print(result.head())


import matplotlib.pyplot as plt
import seaborn as sns
#%matplotlib inline

sns.lmplot('PC1', 'y-axis', data=result, fit_reg=False,
            scatter_kws={'s':50},
            hue='label')

plt.title('PCA result')
plt.show()

print('PCA function from SKLEARN')
from sklearn import decomposition
pca = decomposition.PCA(n_components=1)
sklearn_pca_x = pca.fit_transform(x_std)
sklearn_result = pd.DataFrame(sklearn_pca_x, columns=['PC1'])
sklearn_result['y-axis'] = Y
sklearn_result['label'] = Y

sns.lmplot('PC1', 'y-axis', data=sklearn_result, fit_reg=False, scatter_kws={'s':50}, hue='label')
plt.title('PCA func result')
plt.show()


print('EXECUTION COMPLETE!')