import pandas as pd

folder = '.\\'
csvFile = folder + r'PointZero.csv'
print("CSV File: ", csvFile)

csvData = pd.read_csv(csvFile, sep=',', encoding='utf_8')

df = pd.DataFrame(csvData)
print("DF head: \n", df.head(), "\n")
print("DF shape: ", df.shape, "\n")
print("DF keys: \n", df.keys(), "\n")

from sklearn import preprocessing

df.fillna(0, inplace=True)
le = preprocessing.LabelEncoder()
df['OS version and architecture'] = le.fit_transform(df['OS version and architecture'])
df['Windows Update status'] = le.fit_transform(df['Windows Update status'])
df.drop(['Engine', 'All antispyware', 'All antiviruses'], 1, inplace = True)
print("DF head: ", df.head(), "\n")

x = df.drop('Number of system crashes', axis = 1)
y = df['Number of system crashes']

from sklearn.preprocessing import StandardScaler

scale_x = StandardScaler().fit_transform(x)

from sklearn.decomposition import PCA

pca = PCA()

principalComponents = pca.fit_transform(scale_x)
columns = ["PC" + str(i) for i in range(1, len(x.columns) + 1)] #list(x.columns.values)
print("columns: ", columns, "\n")

principalDataframe = pd.DataFrame(data = principalComponents, columns = columns)

targetDataframe = df[['Number of system crashes']]

newDataframe = pd.concat([principalDataframe, targetDataframe],axis = 1)
print("new data frame:" , newDataframe)

import numpy as np

percent_variance = np.round(pca.explained_variance_ratio_* 100, decimals = 2)
print("percent_variance: " , percent_variance, "\n")

import matplotlib.pyplot as plt

plt.bar(columns, percent_variance)
plt.ylabel('Percentate of Variance Explained')
plt.xlabel('Principal Component')
plt.title('PCA Scree Plot')
plt.show()

xcol = principalDataframe.columns[0]
ycol = principalDataframe.columns[1]

plt.scatter(principalDataframe[xcol].values, principalDataframe[ycol].values)
# plt.scatter(principalDataframe.PC1, principalDataframe.PC2)
plt.title(xcol + " against " + ycol)
plt.xlabel(xcol)
plt.ylabel(ycol)
plt.show()

targets = list(set(y))
print("targets: ", targets, "\n")

colours = []

def rgb_to_hex(rgb):
    return '#%02x%02x%02x' % rgb

import random

for i in range(1, len(set(y)) + 1):
    r = random.randint(0,255)
    g = random.randint(0,255)
    b = random.randint(0,255)
    rgb = (r, g, b)
    print('A Random RGB Value :', rgb, "\n")
    colours.append(rgb_to_hex(rgb))

print("colours: ", colours, "\n")

fig = plt.figure(figsize = (8,8))
ax = fig.add_subplot(1,1,1) 
ax.set_xlabel(xcol)
ax.set_ylabel(ycol)

title = "Plot of " + xcol + " vs " + ycol
ax.set_title(title, fontsize = 20)

for target, colour in zip(targets, colours):
    indicesToKeep = newDataframe['Number of system crashes'] == target
    ax.scatter(newDataframe.loc[indicesToKeep, xcol], newDataframe.loc[indicesToKeep, ycol], c = colour, s = 50)
    
ax.legend(targets)
ax.grid()
plt.show()

print("PCA explained variance ratio: ", pca.explained_variance_ratio_)
